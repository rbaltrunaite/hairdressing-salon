import firebase from 'firebase/app';
import 'firebase/firestore';

var config = {
    apiKey: "AIzaSyDu0wIsO-GwnemZnyi6V09WNys-0Wm2BUI",
    authDomain: "hairdressers-4c916.firebaseapp.com",
    databaseURL: "https://hairdressers-4c916.firebaseio.com",
    projectId: "hairdressers-4c916",
    storageBucket: "hairdressers-4c916.appspot.com",
    messagingSenderId: "784856836318",
    appId: "1:784856836318:web:ea0f45f52e2f53cb"
};
firebase.initializeApp(config);

const db = firebase.firestore();

db.settings({ timestampsInSnapshots: true });

export default db;