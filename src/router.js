import Vue from 'vue';
import Router from 'vue-router';
import Home from './views/Home.vue';
import AboutUs from './views/AboutUs.vue';
import ContactUs from './views/ContactUs.vue';
import Registration from './views/Registration.vue';

Vue.use(Router);

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/aboutus',
      name: 'aboutus',
      component: AboutUs
    },
    {
      path: '/contactus',
      name: 'contactus',
      component: ContactUs
    },
    {
      path: '/registration',
      name: 'registration',
      component: Registration
    }
  ]
})
