import Vue from 'vue';
import Vuex from 'vuex';

import format from 'date-fns/format';

Vue.use(Vuex);

export const store = new Vuex.Store({
    state: {
        date: '',
        time: '',
        id: ''
    },
    getters: {
        formatDate: state => {
            return state.date ? format(state.date, 'YYYY MM DD') : ''
        },
        timeOption: state => {
            return state.time ? state.time : ''
        }
    }
});